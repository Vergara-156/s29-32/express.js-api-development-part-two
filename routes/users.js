const exp = require('express')
const controller = require('../controllers/users')
const auth = require('../auth')
const route = exp.Router()

route.post('/register',(req,res)=>{
	let userData = req.body
	controller.registerUser(userData).then(outcome =>{
		res.send(outcome);
	})
});

route.post('/check-email',(req,res)=>{
	controller.checkEmailExist(req.body).then(outcome=>{
		res.send(outcome)
	})
})
route.get('/all',auth.verify,(req,res) =>{
	let token = req.headers.authorization;
	let payload = auth.decode(token)
	let isAdmin = payload.isAdmin
	isAdmin ? controller.getAllUsers().then(result => 
		res.send(result)) 
	: res.send('Unauthorized User')
	

})
route.get('/details', auth.verify ,(req, res) => {
		let userData = auth.decode(req.headers.authorization);
		let userId = userData.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		});
	});

route.put('/:userId/set-as-admin',(req,res)=>{
	let token =req.headers.authorization
	let isAdmin = auth.decode(token).isAdmin
	let id = req.params;
	(isAdmin) ?  controller.setAsAdmin(id).then(outcome =>
			res.send(outcome))
			: res.send('Unauthorized User')
	})


route.put('/:userId/set-as-user',(req,res)=>{
	let token =req.headers.authorization
	let isAdmin = auth.decode(token).isAdmin
	let id = req.params;
	(isAdmin) ?  controller.setAsNonAdmin(id).then(outcome =>
			res.send(outcome))
			: res.send('Unauthorized User')
	})



route.post('/login', (req, res) => {
		let data = req.body;
		controller.loginUser(data).then(outcome => {
			res.send(outcome);
		});
	});

route.post('/:UserId/change-password', (req,res)=>{
	
	let body = req.body
	controller.updatePassword(body).then(outcome=>{
		res.send(outcome)
	})
})
route.post('/enroll',auth.verify,(req,res)=>{

	let token =req.headers.authorization
	let payload = auth.decode(token)
	let userId = payload.id 
	let isAdmin = payload.isAdmin

	let courseId = req.body.courseId
	
	let data = {
		userId: userId,
		courseId: courseId
	}
	if (!isAdmin) {
		controller.enroll(data).then(outcome =>{
			res.send(outcome)
		})
	
	} else {
	
	}
})
module.exports =route;

