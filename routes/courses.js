const exp = require('express')
const controller = require('../controllers/courses')
const auth = require('../auth')
const route =exp.Router()

route.post('/create', auth.verify, (req,res)=>{
	let isAdmin = auth.decode(req.headers.authorization).isAdmin;
	let data ={
		course: req.body,
	};
	if(isAdmin) {
		controller.addCourse(data).then(outcome => {
 		res.send(outcome)
 	})
	} else {
		res.send('User is not Authorized')
	}
	
})

route.get('/all',auth.verify,(req,res) =>{
	let token = req.headers.authorization;
	let payload = auth.decode(token)
	let isAdmin = payload.isAdmin
	isAdmin ? controller.getAllCourse().then(result => 
		res.send(result)) 
	: res.send('Unauthorized User')
	

})

route.get('/active',(req,res) =>{
	controller.getAllActive().then(resultactive => {
		res.send(resultactive);
	})

})

route.get('/:id',(req,res)=>{
	let data = req.params.id
	controller.getCourse(data).then(result =>{
		res.send(result)
	})
})

route.put('/:courseId',auth.verify,(req,res,)=>{
	
	
	let params = req.params
	let body = req.body
	isAdmin = auth.decode(req.headers.authorization).isAdmin
	if (isAdmin) {
		controller.updateCourse(params,body).then(outcome =>{
		res.send(outcome)
	})
	} else {
		res.send('User not Authorized')
	}
	
})

route.put('/:courseId/archive',auth.verify,(req,res)=>{
	let token =req.headers.authorization
	let isAdmin = auth.decode(token).isAdmin
	let params = req.params;
		(isAdmin) ? 
			controller.archiveCourse(params).then(result=>{
			res.send(result);
	})
			: res.send('Unauthorized User')
})

route.delete('/:courseId/delete',auth.verify,(req,res)=>{
	let token =req.headers.authorization
	let isAdmin = auth.decode(token).isAdmin
	let params = req.params;
		(isAdmin) ? 
			controller.deleteCourse(params).then(result=>{
			res.send(result);
	})
			: res.send('Unauthorized User')
})


module.exports = route;

