const User = require ('../models/User')
const Course = require ('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth')

module.exports.registerUser = (info) => {
	let fName = info.firstName
	let lName = info.lastName
	let uEmail = info.email
	let uPassword = info.password
	let uMobileno = info.mobileNo
	

	let newUser= new User({
		firstName: fName,
		lastName: lName,
		email: uEmail,
		password: bcrypt.hashSync(uPassword,10),
		mobileNo: uMobileno

	})

	return newUser.save().then((saved,err) =>{
		
		if (saved) {
			return saved
		} else {
			return false
		}

		
})
}
module.exports.getAllUsers =()=>{

	return User.find({}).then(searchResult =>{
	return searchResult;

 })
}

module.exports.checkEmailExist =(reqBody)=>{
	
	return User.find({email:reqBody.email}).then(result => {
		if (result.length > 0) {
			return 'Email Already Exist'
		} else {
			return 'Email is Still Available'
		}
	})
}

module.exports.getProfile = (id)=>{
	
	return User.findById(id).then(result =>{
		return result;
	})
}

module.exports.setAsAdmin=(user) =>{
	let id = user.userId
	let updates ={
		isAdmin: true
	}
	return User.findByIdAndUpdate(id,updates).then((admin,err)=>{
			if (admin) {
				return true;
			} else {
				return 'Updates Failed to Implement'
			}
	})
	
}
module.exports.setAsNonAdmin =(user)=>{
	let id = user.userId
	let updates = {
		isAdmin: false
	}
	return User.findByIdAndUpdate(id,updates).then((user,err)=>{
			if (user) {
				return false
			} else {
				'Updates Failed to Implement'
			}
	})
}

module.exports.loginUser = (reqBody) => {
     return User.findOne({email: reqBody.email}).then(result => {
        let uPassW = reqBody.password;
        if (result === null) {
          return false
        } else {
           const isMatched = bcrypt.compareSync(uPassW, result.password); 
           if (isMatched) {
             let dataNiUser = result.toObject();
             return {accessToken: auth.createAccessToken(dataNiUser)};
           } else {
             return false 
           }
        };
     });
  };

module.exports.updatePassword = (id,reqBody) => {

	let  npassword = reqBody.password

	return User.findById(id).then((foundUser,error) =>{

		if (foundUser) {
			foundUser.password = npassword;
			
			return foundUser.save().then((updatedUser,saveErr)=>{
				if (saveErr) {
					return false;
				} else {
					return updatedUser;
				}
			})
		} else {
			return 'No User Found'
		}
	})
}

module.exports.enroll = async (data) => {
	let id = data.userId;
	let course = data.courseId;
	let courseName = data.name;
	let isUserUpdated = await User.findById(id).then(user =>{
	 let userfind = user.enrollments.find(userObj => userObj.courseId ==course)

if (!userfind) {
user.enrollments.push({courseId: course})
		return user.save().then((save,error)=>{
			if (error) {
				return 'Check your CourseId'
			} else {
				return `You enrolled ${courseName} `
			}
		})

			} else {
				console.log('Course Already Enrolled')
				return false
}
			});
	let isCourseUpdated = await Course.findById(course).then(course=>{
		let coursefind = course.enrollees.find(courseObj=> courseObj.userId == id)
		if (!coursefind) {
			course.enrollees.push({userId: id})
		return course.save().then((saved,err)=>{
			if (err) {
				return 'Check your CourseId'
			} else {
				return `You enrolled ${courseName} `
			}
		})
		} else {

			console.log('Course Already Enrolled')
			return 'Course Already Enrolled'
		
	}
		
	});
	if (isUserUpdated && isCourseUpdated) {
			return 'Enrollment Success'
	} else {
		return 'Enrollment Failed, Go to Registrar'
	}

};

