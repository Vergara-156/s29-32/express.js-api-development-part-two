const Course = require ('../models/Course')

module.exports.addCourse = (info) => {
	console.log(info);
	let course = info.course;
	let cName = course.name;
	let cDesc = course.description
	let cCost = course.price;

	let newCourse = new Course({
		name: cName,
		description: cDesc,
		price: cCost
	});

	return newCourse.save().then((savedCourse,err) =>{
		
		if (savedCourse) {
			return savedCourse

		} else {
			return false
		}
	})
 }

module.exports.getAllCourse =()=>{

	return Course.find({}).then(searchResult =>{
	return searchResult;

 })
}

module.exports.getAllActive = () => {

	return Course.find({isActive:true}).then(result =>{
		return result;
	})
}

module.exports.getCourse = (id) =>{
	
	return Course.findById(id).then(result =>{
		return result;
	})

}

module.exports.updateCourse = (course,details)=>{
	let cName =details.name 
	let cDesc = details.description
	let cCost = details.price
	let updateCourse= {
		name:cName,
		description:cDesc,
		price: cCost
	}
	let id = course.courseId;
	return Course.findByIdAndUpdate(id,updateCourse).then((courseUpdated,err)=>{
		if (courseUpdated) {
			return courseUpdated
		} else {
			return 'Failed to update Course'
		}

	})

}
module.exports.archiveCourse =(course)=>{
	let id = course.courseId
	let updates = {
		isActive: false
	}
	return Course.findByIdAndUpdate(id,updates).then((archived,err)=>{
		if (archived) {
			return 'Course archived'
		} else {
			return false
		}

	})
}


module.exports.deleteCourse =(course)=> {
	let id = course.courseId
	return Course.findByIdAndRemove(id).then((removedCourse,err)=>{
		if (removedCourse) {
			return `${removedCourse}Account Deleted Successfully`
		} else {
			return 'No accounts were removed!';
		}

	})
}


