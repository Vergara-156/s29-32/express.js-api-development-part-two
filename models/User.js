const mongoose = require('mongoose')

const userSchema = new mongoose.Schema({
	
	firstName:{
		type: String,
		required: [true, 'First Name is Required']
	},

	lastName:{
		type: String,
		required: [true, 'Last Name is Required']
	},
	email:{
		type: String,
		required: [true, 'email is Required']
	},
	password:{
		type: String,
		required: [true, 'password is Required']
	},
		isAdmin:{
		type: Boolean,
		default: false
	},
	mobileNo:{
		type: String,
		required: [true, 'mobileNo is Required']
	},

	enrollments: [
	  {
   	  courseId: {
   	  	type: String,
	  	required: [true, 'Course ID is required!']
		  },
	  enrolledOn: {
   	  	type: Date,
   	  	default: new Date()
	   	  },
   	  status: {
   	  	type: String,
   	  	default: 'Enrolled'
	   	  }
	   }
	]
}); 

//[SECTION] Model
	module.exports = mongoose.model('User', userSchema);
