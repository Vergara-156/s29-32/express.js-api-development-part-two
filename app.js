	const express = require('express');
	const mongoose = require("mongoose");
	const dotenv = require("dotenv");
	const cors = require("cors");
	const courseRoutes = require('./routes/courses')
	const userRoutes = require('./routes/users')

	dotenv.config();

	const port = process.env.PORT
	const credentials= process.env.MONGO_URL

	const app = express();
	app.use(cors());
	app.use(express.urlencoded({extended: true}));
	app.use(express.json());
	


	mongoose.connect(credentials);
	const db = mongoose.connection
	db.once('open',()=>console.log(`Connected to Atlas`))

	
	app.use('/courses',courseRoutes);
	app.use('/users', userRoutes);
	app.get('/',(req,res)=>{
		res.send('Welcome to my App')
	})
	app.listen(port, () => {
   		console.log(`API is now online on port ${port}`);
	});

